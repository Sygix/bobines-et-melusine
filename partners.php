<?php /* Template Name: Partners */ ?>
<?php get_header(); ?>

<section>
    <div class="mx-auto container py-8">
        <div class="prose-headings:text-xl lg:prose-headings:text-2xl mb-4 lg:mb-8">
            <?= the_content(); ?>
        </div>        

        <?php $partners = get_fields();
        if( $partners ): ?>
            <?php if(!empty($partners['partenaire_0']['nom'])): ?>

                <div class="grid lg:<?= $partners["largeur_de_grille_pc"] ?> md:<?= $partners["largeur_de_grille_tablet"] ?> <?= $partners["largeur_de_grille_mobile"] ?> md:gap-8 gap-4">
                    
                    <?php foreach($partners as $partner):
                        if(!empty($partner['nom'])): ?>

                            <div class="<?= $partner["largeur_mobile"] ?> lg:<?= $partner["largeur"] ?> flex flex-col gap-4 bg-indian-red/20 rounded-2xl p-4">
                                <img class="object-contain max-h-80 max-w-full mx-auto" src="<?= $partner["image"]['url'] ?>" alt="<?= $partner["image"]['alt'] ?>" loading="lazy" width="<?= $partner["image"]['width'] ?>" height="<?= $partner["image"]['height'] ?>">
                                <h2 class="font-medium text-lg lg:text-xl text-center text-indian-red"><?= $partner["nom"] ?></h2>
                                <p class="font-normal text-sm lg:text-base"><?= $partner["description"] ?></p>
                                <div class="flex flex-wrap gap-4">
                                    <?php if(!empty($partner["site_lien0"])) :?>
                                        <a href="<?= $partner["site_lien0"] ?>"><i class="mr-2 fas fa-link"></i><?= $partner["site_texte0"] ?></a> 
                                    <?php endif; ?>
                                    <?php if(!empty($partner["site_lien1"])) :?>
                                        <a href="<?= $partner["site_lien1"] ?>"><i class="mr-2 fas fa-link"></i><?= $partner["site_texte1"] ?></a> 
                                    <?php endif; ?>
                                    <?php if(!empty($partner["site_lien2"])) :?>
                                        <a href="<?= $partner["site_lien2"] ?>"><i class="mr-2 fas fa-link"></i><?= $partner["site_texte2"] ?></a> 
                                    <?php endif; ?>
                                </div>
                            </div>

                        <?php endif;
                    endforeach; ?>

                </div>

            <?php endif; 
        endif; ?>
    </div>
</section>

<!--<div class="col-span-full col-span-3 col-span-2 col-span-1 grid-cols-1 grid-cols-2 grid-cols-3 grid-cols-4 grid-cols-5 grid-cols-6 grid-cols-7 grid-cols-8 grid-cols-9 grid-cols-10 grid-cols-11 grid-cols-12
md:grid-cols-1 md:grid-cols-2 md:grid-cols-3 md:grid-cols-4 md:grid-cols-5 md:grid-cols-6 md:grid-cols-7 md:grid-cols-8 md:grid-cols-9 md:grid-cols-10 md:grid-cols-11 md:grid-cols-12
lg:grid-cols-1 lg:grid-cols-2 lg:grid-cols-3 lg:grid-cols-4 lg:grid-cols-5 lg:grid-cols-6 lg:grid-cols-7 lg:grid-cols-8 lg:grid-cols-9 lg:grid-cols-10 lg:grid-cols-11 lg:grid-cols-12">-->

<?php get_template_part( 'template-parts/cta', 'newsletter' ); ?>

<?php
get_footer();
