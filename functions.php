<?php

/**
 * Theme setup.
 */
function bobines_et_melusine_setup() {
	add_theme_support( 'title-tag' );

	register_nav_menus(
		array(
			'primary' => __( 'Primary Menu', 'tailpress' ),
			'footer' => __( 'Footer Menu', 'tailpress' ),
		)
	);

	add_theme_support(
		'html5',
		array(
			'search-form',
			'comment-form',
			'comment-list',
			'gallery',
			'caption',
		)
	);

    add_theme_support( 'custom-logo' );
	add_theme_support( 'post-thumbnails' );

	add_theme_support( 'align-wide' );
	add_theme_support( 'wp-block-styles' );

	add_theme_support( 'editor-styles' );
	add_editor_style( 'css/editor-style.css' );
}

add_action( 'after_setup_theme', 'bobines_et_melusine_setup' );

/**
 * Enqueue theme assets.
 */
function bobines_et_melusine_enqueue_scripts() {
	$theme = wp_get_theme();

	if(is_page()){
		global $wp_query;
		$template_name = get_post_meta( $wp_query->post->ID, '_wp_page_template', true );
		if($template_name == 'contact.php'){
			wp_enqueue_style('leaflet', 'https://cdn.jsdelivr.net/npm/leaflet@1.9.1/dist/leaflet.css', array(), $theme->get( 'Version' ));
			wp_enqueue_script('leaflet-script', 'https://cdn.jsdelivr.net/npm/leaflet@1.9.1/dist/leaflet.js', array(), $theme->get( 'Version' ));
		}
		if(is_front_page()){
			wp_enqueue_style('swiperjs', 'https://cdn.jsdelivr.net/npm/swiper@8/swiper-bundle.min.css', array(), $theme->get( 'Version' ));
			wp_enqueue_script( 'swiperjs', 'https://cdn.jsdelivr.net/npm/swiper@8/swiper-bundle.min.js', array(), $theme->get( 'Version' ) );
		}
	}

	wp_enqueue_style('aos', 'https://cdn.jsdelivr.net/npm/aos@2.3.4/dist/aos.min.css', array(), $theme->get( 'Version' ));
	wp_enqueue_script('aos', 'https://cdn.jsdelivr.net/npm/aos@2.3.4/dist/aos.min.js', array(), $theme->get( 'Version' ));
	wp_enqueue_style('fontawesome', 'https://cdn.jsdelivr.net/npm/@fortawesome/fontawesome-free@5.15.4/css/all.min.css', array(), $theme->get( 'Version' ));
	wp_enqueue_style( 'tailpress', bobines_et_melusine_asset( 'css/app.css' ), array(), $theme->get( 'Version' ) );
	wp_enqueue_script( 'tailpress', bobines_et_melusine_asset( 'js/app.js' ), array(), $theme->get( 'Version' ) );
	$translation_array = array( 'templateUrl' => get_stylesheet_directory_uri() );
	wp_localize_script( 'tailpress', 'wpObject', $translation_array );
}

add_action( 'wp_enqueue_scripts', 'bobines_et_melusine_enqueue_scripts' );

/**
 * Get asset path.
 *
 * @param string  $path Path to asset.
 *
 * @return string
 */
function bobines_et_melusine_asset( $path ) {
	if ( wp_get_environment_type() === 'production' ) {
		return get_stylesheet_directory_uri() . '/' . $path;
	}

	return add_query_arg( 'time', time(),  get_stylesheet_directory_uri() . '/' . $path );
}

/**
 * Adds option 'li_class' to 'wp_nav_menu'.
 *
 * @param string  $classes String of classes.
 * @param mixed   $item The curren item.
 * @param WP_Term $args Holds the nav menu arguments.
 *
 * @return array
 */
function bobines_et_melusine_nav_menu_add_li_class( $classes, $item, $args, $depth ) {
	if ( isset( $args->li_class ) ) {
		$classes[] = $args->li_class;
	}

	if ( isset( $args->{"li_class_$depth"} ) ) {
		$classes[] = $args->{"li_class_$depth"};
	}

	return $classes;
}

add_filter( 'nav_menu_css_class', 'bobines_et_melusine_nav_menu_add_li_class', 10, 4 );

/**
 * Adds option 'submenu_class' to 'wp_nav_menu'.
 *
 * @param string  $classes String of classes.
 * @param mixed   $item The curren item.
 * @param WP_Term $args Holds the nav menu arguments.
 *
 * @return array
 */
function bobines_et_melusine_nav_menu_add_submenu_class( $classes, $args, $depth ) {
	if ( isset( $args->submenu_class ) ) {
		$classes[] = $args->submenu_class;
	}

	if ( isset( $args->{"submenu_class_$depth"} ) ) {
		$classes[] = $args->{"submenu_class_$depth"};
	}

	return $classes;
}

add_filter( 'nav_menu_submenu_css_class', 'bobines_et_melusine_nav_menu_add_submenu_class', 10, 3 );

// Add custom settings
function theme_settings_page(){
	?>
	    <div class="wrap">
	    <h1>Theme Panel</h1>
	    <form method="post" action="options.php">
	        <?php
	            settings_fields("section");
	            do_settings_sections("theme-options");      
	            submit_button(); 
	        ?>          
	    </form>
		</div>
	<?php
}
function add_theme_menu_item(){
	add_menu_page("Theme Panel", "Theme Panel", "manage_options", "theme-panel", "theme_settings_page", null, 99);
}
add_action("admin_menu", "add_theme_menu_item");
function display_newsletter_element(){
	?>
    	<textarea rows="1" cols="75" type="textarea" name="newsletter_shortcode" id="newsletter_shortcode" placeholder="[newsletter_form form='1']"><?php echo get_option('newsletter_shortcode'); ?></textarea>
    <?php
}
function display_contactform_element(){
	?>
    	<textarea rows="4" cols="75" type="textarea" name="contactform_shortcode" id="contactform_shortcode" placeholder="[contact-form-7 id='232' title='Formulaire']"><?php echo get_option('contactform_shortcode'); ?></textarea>
    <?php
}
function display_contactformwide_element(){
	?>
    	<textarea rows="4" cols="75" type="textarea" name="contactformwide_shortcode" id="contactformwide_shortcode" placeholder="[contact-form-7 id='233' title='Formulaire large' html_class='mx-auto max-w-2xl p-8 rounded-2xl flex flex-col items-center gap-4']"><?php echo get_option('contactformwide_shortcode'); ?></textarea>
    <?php
}
function display_theme_panel_fields(){
	add_settings_section("section", "All Settings", null, "theme-options");
	
	add_settings_field("newsletter_shortcode", "Newsletter Shortcode", "display_newsletter_element", "theme-options", "section");
	add_settings_field("contactform_shortcode", "ContactForm Shortcode", "display_contactform_element", "theme-options", "section");
	add_settings_field("contactformwide_shortcode", "ContactFormWide Shortcode", "display_contactformwide_element", "theme-options", "section");

    register_setting("section", "newsletter_shortcode");
    register_setting("section", "contactform_shortcode");
    register_setting("section", "contactformwide_shortcode");
}
add_action("admin_init", "display_theme_panel_fields");

// Disable ContactForm7 auto P
add_filter('wpcf7_autop_or_not', '__return_false');

//ADD CPT
function custom_post_type() {

	//Social Menu
	$labelsSocial = array(
		'name'                => _x( 'Réseaux sociaux', 'Post Type General Name', 'tailpress' ),
		'menu_name'           => __( 'Réseaux sociaux', 'tailpress' ),
		'add_new_item'        => __( 'Ajouter un nouveau réseau', 'tailpress' ),
		'add_new'             => __( 'Ajouter un réseau', 'tailpress' ),
	);
	$argsSocial = array(
		'label'               => __( 'social-network', 'tailpress' ),
		'labels'              => $labelsSocial,
		'supports'            => array( 'title', 'custom-fields', ),
		'hierarchical'        => false,
		'public'              => true,
		'show_ui'             => true,
		'show_in_menu'        => true,
		'show_in_nav_menus'   => true,
		'show_in_admin_bar'   => true,
		'menu_position'       => 30,
		'menu_icon'           => 'dashicons-share',
		'can_export'          => true,
		'has_archive'         => true,
		'exclude_from_search' => false,
		'publicly_queryable'  => true,
		'capability_type'     => 'post',
		'show_in_rest' => true,
	);
	register_post_type( 'social-network', $argsSocial );
}

add_action( 'init', 'custom_post_type', 0 );

// Add acf fields
function my_acf_add_local_field_groups() {

	// Reseaux sociaux fields
	acf_add_local_field_group(array(
		'key' => 'group_63249864f0de8',
		'title' => 'Réseaux sociaux',
		'fields' => array(
			array(
				'key' => 'field_63249877fc697',
				'label' => 'Icone',
				'name' => 'icone',
				'type' => 'text',
				'instructions' => 'Ajouter uniquement les classes
	Liste des icones disponible ici : https://fontawesome.com/v5/search',
				'required' => 0,
				'conditional_logic' => 0,
				'wrapper' => array(
					'width' => '',
					'class' => '',
					'id' => '',
				),
				'default_value' => '',
				'placeholder' => 'fab fa-tiktok',
				'prepend' => '',
				'append' => '',
				'maxlength' => '',
			),
			array(
				'key' => 'field_63249917fc698',
				'label' => 'Lien',
				'name' => 'lien',
				'type' => 'text',
				'instructions' => '',
				'required' => 0,
				'conditional_logic' => 0,
				'wrapper' => array(
					'width' => '',
					'class' => '',
					'id' => '',
				),
				'default_value' => '',
				'placeholder' => '',
				'prepend' => '',
				'append' => '',
				'maxlength' => '',
			),
			array(
				'key' => 'field_6324992cfc699',
				'label' => 'Texte',
				'name' => 'texte',
				'type' => 'text',
				'instructions' => '',
				'required' => 1,
				'conditional_logic' => 0,
				'wrapper' => array(
					'width' => '',
					'class' => '',
					'id' => '',
				),
				'default_value' => 'no text',
				'placeholder' => 'no text',
				'prepend' => '',
				'append' => '',
				'maxlength' => '',
			),
		),
		'location' => array(
			array(
				array(
					'param' => 'post_type',
					'operator' => '==',
					'value' => 'social-network',
				),
			),
		),
		'menu_order' => 0,
		'position' => 'normal',
		'style' => 'default',
		'label_placement' => 'top',
		'instruction_placement' => 'label',
		'hide_on_screen' => '',
		'active' => true,
		'description' => '',
		'show_in_rest' => 0,
	));

	// Function to generate more fields in Accueil Gallery
	function moreGalleryItems($max){
		$array = array();
		for( $i=0; $i<$max ; $i++ ){
			$item = array();
			$item['key'] = 'field_galleryitem'.$i;
			$item['label'] = 'item '.$i;
			$item['name'] = 'item_'.$i;
			$item['type'] = 'group';
			$item['instructions'] = '';
			$item['required'] = 0;

			if($i > 0){
				$minus1 = $i-1;
				$item['conditional_logic'] = [[[
					'field' => 'field_galleryitem'.$minus1.'titre',
					'operator' => '!=empty',
				]]];
			}else{
				$item['conditional_logic'] = 0;
			}
			
			$item['layout'] = 'table';
			$item['wrapper'] = [
				'width' => '',
				'class' => '',
				'id' => '',
			];
			
			$item['sub_fields'] = [
				[
					'key' => 'field_galleryitem'.$i.'titre',
					'label' => 'titre',
					'name' => 'titre',
					'type' => 'text',
					'instructions' => '',
					'required' => 0,
					'conditional_logic' => 0,
					'wrapper' => array(
						'width' => '',
						'class' => '',
						'id' => '',
					),
					'default_value' => '',
					'placeholder' => '',
					'prepend' => '',
					'append' => '',
					'maxlength' => '',
				],
				[
					'key' => 'field_galleryitem'.$i.'image',
					'label' => 'image',
					'name' => 'image',
					'type' => 'image',
					'instructions' => '',
					'required' => 0,
					'conditional_logic' => 0,
					'wrapper' => array(
						'width' => '',
						'class' => '',
						'id' => '',
					),
					'return_format' => 'array',
					'preview_size' => 'thumbnail',
					'library' => 'all',
					'min_width' => '',
					'min_height' => '',
					'min_size' => '',
					'max_width' => '',
					'max_height' => '',
					'max_size' => '',
					'mime_types' => '',
				],
				[
					'key' => 'field_galleryitem'.$i.'description',
					'label' => 'description',
					'name' => 'description',
					'type' => 'textarea',
					'instructions' => '',
					'required' => 0,
					'conditional_logic' => 0,
					'wrapper' => array(
						'width' => '',
						'class' => '',
						'id' => '',
					),
					'default_value' => '',
					'placeholder' => '',
					'maxlength' => '',
					'rows' => '',
					'new_lines' => '',
				],
				[
					'key' => 'field_galleryitem'.$i.'lien',
					'label' => 'lien',
					'name' => 'lien',
					'type' => 'link',
					'instructions' => '',
					'required' => 0,
					'conditional_logic' => 0,
					'wrapper' => array(
						'width' => '',
						'class' => '',
						'id' => '',
					),
					'return_format' => 'array',
				]
			];
			
			array_push($array, $item);
		}

		return $array;
	}

	// Function to generate more fields in Accueil Grid
	function moreGridItems($max){
		$array = array();
		for( $i=0; $i<$max ; $i++ ){
			$item = array();
			$item['key'] = 'field_griditem'.$i;
			$item['label'] = 'item '.$i;
			$item['name'] = 'item_'.$i;
			$item['type'] = 'group';
			$item['instructions'] = '';
			$item['required'] = 0;

			if($i > 0){
				$minus1 = $i-1;
				$item['conditional_logic'] = [[[
					'field' => 'field_griditem'.$minus1.'titre',
					'operator' => '!=empty',
				]]];
			}else{
				$item['conditional_logic'] = 0;
			}
			
			$item['layout'] = 'table';
			$item['wrapper'] = [
				'width' => '',
				'class' => '',
				'id' => '',
			];
			
			$item['sub_fields'] = [
				[
					'key' => 'field_griditem'.$i.'titre',
					'label' => 'titre',
					'name' => 'titre',
					'type' => 'text',
					'instructions' => '',
					'required' => 0,
					'conditional_logic' => 0,
					'wrapper' => array(
						'width' => '',
						'class' => '',
						'id' => '',
					),
					'default_value' => '',
					'placeholder' => '',
					'prepend' => '',
					'append' => '',
					'maxlength' => '',
				],
				[
					'key' => 'field_griditem'.$i.'image',
					'label' => 'image',
					'name' => 'image',
					'type' => 'image',
					'instructions' => '',
					'required' => 0,
					'conditional_logic' => 0,
					'wrapper' => array(
						'width' => '',
						'class' => '',
						'id' => '',
					),
					'return_format' => 'array',
					'preview_size' => 'thumbnail',
					'library' => 'all',
					'min_width' => '',
					'min_height' => '',
					'min_size' => '',
					'max_width' => '',
					'max_height' => '',
					'max_size' => '',
					'mime_types' => '',
				],
				[
					'key' => 'field_griditem'.$i.'description',
					'label' => 'description',
					'name' => 'description',
					'type' => 'textarea',
					'instructions' => '',
					'required' => 0,
					'conditional_logic' => 0,
					'wrapper' => array(
						'width' => '',
						'class' => '',
						'id' => '',
					),
					'default_value' => '',
					'placeholder' => '',
					'maxlength' => '',
					'rows' => '',
					'new_lines' => '',
				]
			];
			
			array_push($array, $item);
		}

		return $array;
	}

	// Function to generate more fields in parteners
	function moreParteners($max){
		$array = array();
		array_push($array, [
			'key' => 'field_632c95128966c',
			'label' => 'largeur de grille (pc)',
			'name' => 'largeur_de_grille_pc',
			'type' => 'select',
			'instructions' => '',
			'required' => 1,
			'conditional_logic' => 0,
			'wrapper' => array(
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'choices' => array(
				'grid-cols-1' => 'grid-cols-1',
				'grid-cols-2' => 'grid-cols-2',
				'grid-cols-3' => 'grid-cols-3',
				'grid-cols-4' => 'grid-cols-4',
				'grid-cols-5' => 'grid-cols-5',
				'grid-cols-6' => 'grid-cols-6',
				'grid-cols-7' => 'grid-cols-7',
				'grid-cols-8' => 'grid-cols-8',
				'grid-cols-9' => 'grid-cols-9',
				'grid-cols-10' => 'grid-cols-10',
				'grid-cols-12' => 'grid-cols-12',
			),
			'default_value' => 'grid-cols-4',
			'return_format' => 'value',
			'multiple' => 0,
			'allow_null' => 0,
			'ui' => 0,
			'ajax' => 0,
			'placeholder' => '',
		]);
		array_push($array, [
			'key' => 'field_632c53846534c',
			'label' => 'largeur de grille (tablet)',
			'name' => 'largeur_de_grille_tablet',
			'type' => 'select',
			'instructions' => '',
			'required' => 1,
			'conditional_logic' => 0,
			'wrapper' => array(
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'choices' => array(
				'grid-cols-1' => 'grid-cols-1',
				'grid-cols-2' => 'grid-cols-2',
				'grid-cols-3' => 'grid-cols-3',
				'grid-cols-4' => 'grid-cols-4',
				'grid-cols-5' => 'grid-cols-5',
				'grid-cols-6' => 'grid-cols-6',
				'grid-cols-7' => 'grid-cols-7',
				'grid-cols-8' => 'grid-cols-8',
				'grid-cols-9' => 'grid-cols-9',
				'grid-cols-10' => 'grid-cols-10',
				'grid-cols-12' => 'grid-cols-12',
			),
			'default_value' => 'grid-cols-2',
			'return_format' => 'value',
			'multiple' => 0,
			'allow_null' => 0,
			'ui' => 0,
			'ajax' => 0,
			'placeholder' => '',
		]);
		array_push($array, [
			'key' => 'field_632c76425498c',
			'label' => 'largeur de grille (mobile)',
			'name' => 'largeur_de_grille_mobile',
			'type' => 'select',
			'instructions' => '',
			'required' => 1,
			'conditional_logic' => 0,
			'wrapper' => array(
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'choices' => array(
				'grid-cols-1' => 'grid-cols-1',
				'grid-cols-2' => 'grid-cols-2',
				'grid-cols-3' => 'grid-cols-3',
				'grid-cols-4' => 'grid-cols-4',
				'grid-cols-5' => 'grid-cols-5',
				'grid-cols-6' => 'grid-cols-6',
				'grid-cols-7' => 'grid-cols-7',
				'grid-cols-8' => 'grid-cols-8',
				'grid-cols-9' => 'grid-cols-9',
				'grid-cols-10' => 'grid-cols-10',
				'grid-cols-12' => 'grid-cols-12',
			),
			'default_value' => 'grid-cols-1',
			'return_format' => 'value',
			'multiple' => 0,
			'allow_null' => 0,
			'ui' => 0,
			'ajax' => 0,
			'placeholder' => '',
		]);

		for( $i=0; $i<$max ; $i++ ){
			$item = array();
			$item['key'] = 'field_partener'.$i;
			$item['label'] = 'Partenaire '.$i;
			$item['name'] = 'partenaire_'.$i;
			$item['type'] = 'group';
			$item['instructions'] = '';
			$item['required'] = 0;

			if($i > 0){
				$minus1 = $i-1;
				$item['conditional_logic'] = [[[
					'field' => 'field_partener'.$minus1.'nom',
					'operator' => '!=empty',
				]]];
			}else{
				$item['conditional_logic'] = 0;
			}
			
			$item['layout'] = 'table';
			$item['wrapper'] = [
				'width' => '',
				'class' => '',
				'id' => '',
			];
			
			$item['sub_fields'] = [
				[
					'key' => 'field_partener'.$i.'nom',
					'label' => 'nom',
					'name' => 'nom',
					'type' => 'text',
					'instructions' => '',
					'required' => 0,
					'conditional_logic' => 0,
					'wrapper' => array(
						'width' => '',
						'class' => '',
						'id' => '',
					),
					'default_value' => '',
					'placeholder' => '',
					'prepend' => '',
					'append' => '',
					'maxlength' => '',
				],
				[
					'key' => 'field_partener'.$i.'image',
					'label' => 'image',
					'name' => 'image',
					'type' => 'image',
					'instructions' => '',
					'required' => 0,
					'conditional_logic' => 0,
					'wrapper' => array(
						'width' => '',
						'class' => '',
						'id' => '',
					),
					'return_format' => 'array',
					'preview_size' => 'thumbnail',
					'library' => 'all',
					'min_width' => '',
					'min_height' => '',
					'min_size' => '',
					'max_width' => '',
					'max_height' => '',
					'max_size' => '',
					'mime_types' => '',
				],
				[
					'key' => 'field_partener'.$i.'description',
					'label' => 'description',
					'name' => 'description',
					'type' => 'textarea',
					'instructions' => '',
					'required' => 0,
					'conditional_logic' => 0,
					'wrapper' => array(
						'width' => '',
						'class' => '',
						'id' => '',
					),
					'default_value' => '',
					'placeholder' => '',
					'maxlength' => '',
					'rows' => '',
					'new_lines' => '',
				],
				[
					'key' => 'field_partener'.$i.'sitelien0',
					'label' => 'site lien 0',
					'name' => 'site_lien0',
					'type' => 'url',
					'instructions' => '',
					'required' => 0,
					'conditional_logic' => 0,
					'wrapper' => array(
						'width' => '',
						'class' => '',
						'id' => '',
					),
					'default_value' => '',
					'placeholder' => '',
				],
				[
					'key' => 'field_partener'.$i.'sitetexte0',
					'label' => 'site texte 0',
					'name' => 'site_texte0',
					'type' => 'text',
					'instructions' => '',
					'required' => 0,
					'conditional_logic' => 0,
					'wrapper' => array(
						'width' => '',
						'class' => '',
						'id' => '',
					),
					'default_value' => '',
					'maxlength' => '',
					'placeholder' => '',
					'prepend' => '',
					'append' => '',
				],
				[
					'key' => 'field_partener'.$i.'sitelien1',
					'label' => 'site lien 1',
					'name' => 'site_lien1',
					'type' => 'url',
					'instructions' => '',
					'required' => 0,
					'conditional_logic' => 0,
					'wrapper' => array(
						'width' => '',
						'class' => '',
						'id' => '',
					),
					'default_value' => '',
					'placeholder' => '',
				],
				[
					'key' => 'field_partener'.$i.'sitetexte1',
					'label' => 'site texte 1',
					'name' => 'site_texte1',
					'type' => 'text',
					'instructions' => '',
					'required' => 0,
					'conditional_logic' => 0,
					'wrapper' => array(
						'width' => '',
						'class' => '',
						'id' => '',
					),
					'default_value' => '',
					'maxlength' => '',
					'placeholder' => '',
					'prepend' => '',
					'append' => '',
				],
				[
					'key' => 'field_partener'.$i.'sitelien2',
					'label' => 'site lien 2',
					'name' => 'site_lien2',
					'type' => 'url',
					'instructions' => '',
					'required' => 0,
					'conditional_logic' => 0,
					'wrapper' => array(
						'width' => '',
						'class' => '',
						'id' => '',
					),
					'default_value' => '',
					'placeholder' => '',
				],
				[
					'key' => 'field_partener'.$i.'sitetexte2',
					'label' => 'site texte 2',
					'name' => 'site_texte2',
					'type' => 'text',
					'instructions' => '',
					'required' => 0,
					'conditional_logic' => 0,
					'wrapper' => array(
						'width' => '',
						'class' => '',
						'id' => '',
					),
					'default_value' => '',
					'maxlength' => '',
					'placeholder' => '',
					'prepend' => '',
					'append' => '',
				],
				[
					'key' => 'field_partener'.$i.'largeur',
						'label' => 'largeur pc',
						'name' => 'largeur',
						'type' => 'select',
						'instructions' => '',
						'required' => 1,
						'conditional_logic' => 0,
						'wrapper' => array(
							'width' => '',
							'class' => '',
							'id' => '',
						),
						'choices' => array(
							'col-span-1' => 'col-span-1',
							'col-span-2' => 'col-span-2',
							'col-span-3' => 'col-span-3',
							'col-span-full' => 'col-span-full',
						),
						'default_value' => 'col-span-1',
						'return_format' => 'value',
						'multiple' => 0,
						'allow_null' => 0,
						'ui' => 0,
						'ajax' => 0,
						'placeholder' => '',
				],
				[
					'key' => 'field_partener'.$i.'largeurmobile',
						'label' => 'largeur mobile',
						'name' => 'largeur_mobile',
						'type' => 'select',
						'instructions' => '',
						'required' => 1,
						'conditional_logic' => 0,
						'wrapper' => array(
							'width' => '',
							'class' => '',
							'id' => '',
						),
						'choices' => array(
							'col-span-1' => 'col-span-1',
							'col-span-2' => 'col-span-2',
							'col-span-3' => 'col-span-3',
							'col-span-full' => 'col-span-full',
						),
						'default_value' => 'col-span-1',
						'return_format' => 'value',
						'multiple' => 0,
						'allow_null' => 0,
						'ui' => 0,
						'ajax' => 0,
						'placeholder' => '',
				]
			];
			
			array_push($array, $item);
		}

		return $array;
	}

	// Accueil fields
	acf_add_local_field_group(array(
		'key' => 'group_6329b9731c937',
		'title' => 'Accueil',
		'fields' => array(
			array(
				'key' => 'field_6329b9ba458b0',
				'label' => 'Gallerie',
				'name' => 'gallerie',
				'type' => 'group',
				'instructions' => '',
				'required' => 0,
				'conditional_logic' => 0,
				'wrapper' => array(
					'width' => '',
					'class' => '',
					'id' => '',
				),
				'layout' => 'row',
				'sub_fields' => moreGalleryItems(25),
			),
			array(
				'key' => 'field_6329d35600e1f',
				'label' => 'Grille',
				'name' => 'grille',
				'type' => 'group',
				'instructions' => '',
				'required' => 0,
				'conditional_logic' => 0,
				'wrapper' => array(
					'width' => '',
					'class' => '',
					'id' => '',
				),
				'layout' => 'row',
				'sub_fields' => moreGridItems(10),
			),
			array(
				'key' => 'field_632b16b73a364',
				'label' => 'Créations',
				'name' => 'creations',
				'type' => 'group',
				'instructions' => '',
				'required' => 0,
				'conditional_logic' => 0,
				'wrapper' => array(
					'width' => '',
					'class' => '',
					'id' => '',
				),
				'layout' => 'row',
				'sub_fields' => array(
					array(
						'key' => 'field_632b18a654b80',
						'label' => 'titre',
						'name' => 'titre',
						'type' => 'text',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => 0,
						'wrapper' => array(
							'width' => '',
							'class' => '',
							'id' => '',
						),
						'default_value' => '',
						'placeholder' => '',
						'prepend' => '',
						'append' => '',
						'maxlength' => '',
					),
					array(
						'key' => 'field_632b16de3a365',
						'label' => 'Produits',
						'name' => 'produits',
						'type' => 'post_object',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => 0,
						'wrapper' => array(
							'width' => '',
							'class' => '',
							'id' => '',
						),
						'post_type' => array(
							0 => 'product',
							1 => 'product_variation',
						),
						'taxonomy' => '',
						'allow_null' => 0,
						'multiple' => 1,
						'return_format' => 'object',
						'ui' => 1,
					),
					array(
						'key' => 'field_632b170e3a366',
						'label' => 'plus',
						'name' => 'plus',
						'type' => 'text',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => 0,
						'wrapper' => array(
							'width' => '',
							'class' => '',
							'id' => '',
						),
						'default_value' => '',
						'placeholder' => '',
						'prepend' => '',
						'append' => '',
						'maxlength' => '',
					),
					array(
						'key' => 'field_632b171c3a367',
						'label' => 'plus lien',
						'name' => 'plus_lien',
						'type' => 'page_link',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => 0,
						'wrapper' => array(
							'width' => '',
							'class' => '',
							'id' => '',
						),
						'post_type' => '',
						'taxonomy' => '',
						'allow_null' => 0,
						'allow_archives' => 0,
						'multiple' => 0,
					),
				),
			),
			array(
				'key' => 'field_6329ecf05eccd',
				'label' => 'Confection',
				'name' => 'confection',
				'type' => 'group',
				'instructions' => '',
				'required' => 0,
				'conditional_logic' => 0,
				'wrapper' => array(
					'width' => '',
					'class' => '',
					'id' => '',
				),
				'layout' => 'row',
				'sub_fields' => array(
					array(
						'key' => 'field_6329ed0f5ecd6',
						'label' => 'image gauche',
						'name' => 'image_gauche',
						'type' => 'image',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => 0,
						'wrapper' => array(
							'width' => '',
							'class' => '',
							'id' => '',
						),
						'return_format' => 'array',
						'preview_size' => 'thumbnail',
						'library' => 'all',
						'min_width' => '',
						'min_height' => '',
						'min_size' => '',
						'max_width' => '',
						'max_height' => '',
						'max_size' => '',
						'mime_types' => '',
					),
					array(
						'key' => 'field_6329ed235ecd7',
						'label' => 'titre',
						'name' => 'titre',
						'type' => 'text',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => 0,
						'wrapper' => array(
							'width' => '',
							'class' => '',
							'id' => '',
						),
						'default_value' => '',
						'placeholder' => '',
						'prepend' => '',
						'append' => '',
						'maxlength' => '',
					),
					array(
						'key' => 'field_6329ed345ecd8',
						'label' => 'description',
						'name' => 'description',
						'type' => 'textarea',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => 0,
						'wrapper' => array(
							'width' => '',
							'class' => '',
							'id' => '',
						),
						'default_value' => '',
						'placeholder' => '',
						'maxlength' => '',
						'rows' => '',
						'new_lines' => '',
					),
					array(
						'key' => 'field_6329eda95ecd9',
						'label' => 'lien',
						'name' => 'lien',
						'type' => 'page_link',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => 0,
						'wrapper' => array(
							'width' => '',
							'class' => '',
							'id' => '',
						),
						'post_type' => array(
							0 => 'page',
							1 => 'post',
							2 => 'product',
							3 => 'social-network',
						),
						'taxonomy' => '',
						'allow_null' => 0,
						'allow_archives' => 0,
						'multiple' => 0,
					),
					array(
						'key' => 'field_6329ede25ecda',
						'label' => 'lien texte',
						'name' => 'lien_texte',
						'type' => 'text',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => 0,
						'wrapper' => array(
							'width' => '',
							'class' => '',
							'id' => '',
						),
						'default_value' => '',
						'placeholder' => '',
						'prepend' => '',
						'append' => '',
						'maxlength' => '',
					),
					array(
						'key' => 'field_6329edf75ecdb',
						'label' => 'image droite',
						'name' => 'image_droite',
						'type' => 'image',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => 0,
						'wrapper' => array(
							'width' => '',
							'class' => '',
							'id' => '',
						),
						'return_format' => 'array',
						'preview_size' => 'thumbnail',
						'library' => 'all',
						'min_width' => '',
						'min_height' => '',
						'min_size' => '',
						'max_width' => '',
						'max_height' => '',
						'max_size' => '',
						'mime_types' => '',
					),
				),
			),
			array(
				'key' => 'field_6331a80e701ed',
				'label' => 'produits categorie',
				'name' => 'produits_categorie',
				'type' => 'post_object',
				'instructions' => '',
				'required' => 0,
				'conditional_logic' => 0,
				'wrapper' => array(
					'width' => '',
					'class' => '',
					'id' => '',
				),
				'post_type' => array(
					0 => 'product',
				),
				'taxonomy' => 0,
				'allow_null' => 1,
				'multiple' => 1,
				'return_format' => 'object',
				'ui' => 1,
			),
		),
		'location' => array(
			array(
				array(
					'param' => 'page_type',
					'operator' => '==',
					'value' => 'front_page',
				),
			),
		),
		'menu_order' => 0,
		'position' => 'acf_after_title',
		'style' => 'seamless',
		'label_placement' => 'top',
		'instruction_placement' => 'label',
		'hide_on_screen' => array(
			0 => 'permalink',
			1 => 'the_content',
			2 => 'excerpt',
			3 => 'discussion',
			4 => 'comments',
			5 => 'revisions',
			6 => 'slug',
			7 => 'author',
			8 => 'format',
			9 => 'page_attributes',
			10 => 'featured_image',
			11 => 'categories',
			12 => 'tags',
			13 => 'send-trackbacks',
		),
		'active' => true,
		'description' => '',
		'show_in_rest' => 0,
	));

	// Contact fields
	acf_add_local_field_group(array(
		'key' => 'group_632afb2202955',
		'title' => 'Contact',
		'fields' => array(
			array(
				'key' => 'field_632afb6628875',
				'label' => 'map',
				'name' => 'map',
				'type' => 'group',
				'instructions' => '',
				'required' => 0,
				'conditional_logic' => 0,
				'wrapper' => array(
					'width' => '',
					'class' => '',
					'id' => '',
				),
				'layout' => 'row',
				'sub_fields' => array(
					array(
						'key' => 'field_632afb8928876',
						'label' => 'Centre',
						'name' => 'centre',
						'type' => 'text',
						'instructions' => '',
						'required' => 1,
						'conditional_logic' => 0,
						'wrapper' => array(
							'width' => '',
							'class' => '',
							'id' => '',
						),
						'default_value' => '48.412,-1.825',
						'placeholder' => '48.412,-1.825',
						'prepend' => '',
						'append' => '',
						'maxlength' => '',
					),
					array(
						'key' => 'field_632afba128877',
						'label' => 'Zoom',
						'name' => 'zoom',
						'type' => 'text',
						'instructions' => '',
						'required' => 1,
						'conditional_logic' => 0,
						'wrapper' => array(
							'width' => '',
							'class' => '',
							'id' => '',
						),
						'default_value' => 16,
						'placeholder' => 16,
						'prepend' => '',
						'append' => '',
						'maxlength' => '',
					),
					array(
						'key' => 'field_632afbc728878',
						'label' => 'Map ID',
						'name' => 'map_id',
						'type' => 'text',
						'instructions' => '',
						'required' => 1,
						'conditional_logic' => 0,
						'wrapper' => array(
							'width' => '',
							'class' => '',
							'id' => '',
						),
						'default_value' => 'bobinesetmelusine/cl8d5zku000an14qlsyg4gell',
						'placeholder' => 'bobinesetmelusine/cl8d5zku000an14qlsyg4gell',
						'prepend' => '',
						'append' => '',
						'maxlength' => '',
					),
					array(
						'key' => 'field_632afbe228879',
						'label' => 'Access Token',
						'name' => 'access_token',
						'type' => 'text',
						'instructions' => '',
						'required' => 1,
						'conditional_logic' => 0,
						'wrapper' => array(
							'width' => '',
							'class' => '',
							'id' => '',
						),
						'default_value' => 'pk.eyJ1IjoiYm9iaW5lc2V0bWVsdXNpbmUiLCJhIjoiY2w4ZGdhang0MDA5YTQzbW80Y21mbndoYSJ9.PpnWgAAySGSEGAhzEd6czw',
						'placeholder' => 'pk.eyJ1IjoiYm9iaW5lc2V0bWVsdXNpbmUiLCJhIjoiY2w4ZGdhang0MDA5YTQzbW80Y21mbndoYSJ9.PpnWgAAySGSEGAhzEd6czw',
						'prepend' => '',
						'append' => '',
						'maxlength' => '',
					),
					array(
						'key' => 'field_632afbf92887a',
						'label' => 'Marker',
						'name' => 'marker',
						'type' => 'text',
						'instructions' => '',
						'required' => 1,
						'conditional_logic' => 0,
						'wrapper' => array(
							'width' => '',
							'class' => '',
							'id' => '',
						),
						'default_value' => '48.4121074,-1.81824724',
						'placeholder' => '48.4121074,-1.81824724',
						'prepend' => '',
						'append' => '',
						'maxlength' => '',
					),
					array(
						'key' => 'field_632afc0e2887b',
						'label' => 'Google map lien',
						'name' => 'google_map_lien',
						'type' => 'url',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => 0,
						'wrapper' => array(
							'width' => '',
							'class' => '',
							'id' => '',
						),
						'default_value' => '',
						'placeholder' => '',
					),
				),
			),
			array(
				'key' => 'field_632afbf456462a',
				'label' => 'Texte du bouton (mobile)',
				'name' => 'btn_text',
				'type' => 'text',
				'instructions' => '',
				'required' => 1,
				'conditional_logic' => 0,
				'wrapper' => array(
					'width' => '',
					'class' => '',
					'id' => '',
				),
				'default_value' => '',
				'placeholder' => '',
				'prepend' => '',
				'append' => '',
				'maxlength' => '',
			),
		),
		'location' => array(
			array(
				array(
					'param' => 'page_template',
					'operator' => '==',
					'value' => 'contact.php',
				),
			),
		),
		'menu_order' => 0,
		'position' => 'normal',
		'style' => 'default',
		'label_placement' => 'top',
		'instruction_placement' => 'label',
		'hide_on_screen' => '',
		'active' => true,
		'description' => '',
		'show_in_rest' => 0,
	));

	// Partners fields
	acf_add_local_field_group(array(
		'key' => 'group_9273b9731c563',
		'title' => 'Partenaires',
		'fields' => moreParteners(25),
		'location' => array(
			array(
				array(
					'param' => 'page_template',
					'operator' => '==',
					'value' => 'partners.php',
				),
			),
		),
		'menu_order' => 0,
		'position' => 'acf_after_title',
		'style' => 'seamless',
		'label_placement' => 'top',
		'instruction_placement' => 'label',
		'hide_on_screen' => '',
		'active' => true,
		'description' => '',
		'show_in_rest' => 0,
	));
	
}

add_action('acf/init', 'my_acf_add_local_field_groups');


/**
 * Adds woocommerce support.
 */
function mytheme_add_woocommerce_support() {
	add_theme_support( 'woocommerce' );
	add_theme_support( 'wc-product-gallery-zoom' );
	add_theme_support( 'wc-product-gallery-lightbox' );
	add_theme_support( 'wc-product-gallery-slider' );
}
add_action( 'after_setup_theme', 'mytheme_add_woocommerce_support' );

// Disable woocommerce reviews
add_filter( 'woocommerce_product_tabs', 'woo_remove_product_tabs', 98 );
function woo_remove_product_tabs( $tabs ) {
    unset( $tabs['reviews'] ); // Remove the reviews tab
    return $tabs;
}

// Change woocommerce breadcrumb separator
add_filter( 'woocommerce_breadcrumb_defaults', 'wcc_change_breadcrumb_delimiter' );
function wcc_change_breadcrumb_delimiter( $defaults ) {
// Change the breadcrumb delimeter from '/' to '>'
$defaults['delimiter'] = ' <i class="fas fa-chevron-right"></i> ';
return $defaults;
}

// To change add to cart text on single product page
add_filter( 'woocommerce_product_single_add_to_cart_text', 'woocommerce_custom_single_add_to_cart_text' ); 
function woocommerce_custom_single_add_to_cart_text() {
    return __( 'Acheter', 'woocommerce' ); 
}

// To change add to cart text on product archives(Collection) page
add_filter( 'woocommerce_product_add_to_cart_text', 'woocommerce_custom_product_add_to_cart_text' );  
function woocommerce_custom_product_add_to_cart_text() {
    return __( 'Acheter', 'woocommerce' );
}

// Add newsletter CTA after category items
add_filter( 'woocommerce_after_shop_loop', 'woocommerce_custom_after_shop_loop');
function woocommerce_custom_after_shop_loop() {
    return get_template_part( 'template-parts/cta', 'newsletter' );
}

// grid_products shortcode
function grid_products($atts) {
    
	// extract attributes
    extract( shortcode_atts( array(
        'products'			=> '',
		'grid_class' => 'gap-4 grid-cols-1 sm:grid-cols-2 lg:grid-cols-4',
        'post_id'		=> false,
        'format_value'	=> true
    ), $atts ) );

	$products = explode(',', $products);

	if(!empty($products)){
		$grid = "<div class='grid $grid_class'>";

		foreach($products as $product){
			$productData = wc_get_product( $product );
			if($productData->catalog_visibility != 'visible' || $productData->status != 'publish') continue;

			$link = get_permalink($productData->get_id());
			$name = $productData->get_name();
			$image = $productData->get_image('woocommerce_medium', array( "class" => "absolute w-full h-full object-cover object-center m-0 rounded-2xl" ));//wp_get_attachment_image( $productData->get_image_id());
			$price = wc_get_price_including_tax( $productData );
			$currency = get_woocommerce_currency_symbol();

			$grid .= "
			<a href='$link' class='aos h-72 w-full relative no-underline text-white rounded-2xl border-2 border-indian-red overflow-hidden shadow-lg'  data-aos='zoom-in' data-aos-once='true'  data-aos-delay='300'>
				$image
				<div class='absolute bottom-0 backdrop-blur-md bg-white/40 w-full p-4'>
					<h2 class='font-medium !text-base lg:!text-lg truncate text-center m-0 mb-4'>$name</h2>
					<h4 class='font-normal !text-sm text-center m-0'>$price $currency</h4>
				</div>
			</a>";
		}
		
		$grid .= "</div>";		
		return $grid;
	}

	return;
}
add_shortcode('grid_products', 'grid_products');


//Disable author pages
add_action('template_redirect', 'my_custom_disable_author_page');
function my_custom_disable_author_page() {
	global $wp_query;

	if ( is_author() ) {
		wp_safe_redirect(get_option('home'), 301);
		exit;
	}
}

//Disable wordpress topbar
add_filter('show_admin_bar', '__return_false');

//Disable comments
// Removes from admin menu
add_action( 'admin_menu', 'my_remove_admin_menus' );
function my_remove_admin_menus() {
	remove_menu_page( 'edit-comments.php' );
}
// Removes from post and pages
add_action('init', 'remove_comment_support', 100);

function remove_comment_support() {
	remove_post_type_support( 'post', 'comments' );
	remove_post_type_support( 'page', 'comments' );
}
// Removes from admin bar
function mytheme_admin_bar_render() {
	global $wp_admin_bar;
	$wp_admin_bar->remove_menu('comments');
}
add_action( 'wp_before_admin_bar_render', 'mytheme_admin_bar_render' );

// Theme update checker
require 'plugin-update-checker/plugin-update-checker.php';
$myUpdateChecker = Puc_v4_Factory::buildUpdateChecker(
	'https://gitlab.com/Sygix/bobines-et-melusine/',
	__FILE__,
	'bobines-et-melusine'
);
$myUpdateChecker->setBranch('master');