<?php get_header(); ?>

<section>
    <div class="prose prose-headings:text-xl lg:prose-headings:text-2xl !container my-8 mx-auto
    prose-headings:font-medium">
        <?php woocommerce_breadcrumb(); ?>
        <?= woocommerce_content(); ?>
    </div>
</section>

<?php
get_footer();
