const theme = require('./theme.json');
const defaultTheme = require('tailwindcss/defaultTheme')
const tailpress = require("@jeffreyvr/tailwindcss-tailpress");

module.exports = {
    content: [
        './*.php',
        './**/*.php',
        './resources/css/*.css',
        './resources/js/*.js',
        './safelist.txt'
    ],
    theme: {
        container: {
            padding: {
                DEFAULT: '1rem',
                sm: '2rem',
            },
        },
        extend: {
            colors: tailpress.colorMapper(tailpress.theme('settings.color.palette', theme)),
            fontSize: tailpress.fontSizeMapper(tailpress.theme('settings.typography.fontSizes', theme)),
            fontFamily: {
                'sans': ['Folio', ...defaultTheme.fontFamily.sans],
                'cookie': 'Cookie'
            },
            transitionProperty: {
                'm-height': 'max-height',
                'm-width': 'max-width',
                'bg-color': 'background-color',
                'border-color': 'border-color',
                'text-color': 'text-color',
            },
            maxHeight: {
                'inherit': 'inherit',
            },
        
        },
        screens: {
            'xs': '480px',
            'sm': '640px',
            'md': '768px',
            'lg': tailpress.theme('settings.layout.contentSize', theme),
            'xl': tailpress.theme('settings.layout.wideSize', theme),
            '2xl': '1536px'
        },
    },
    plugins: [
        tailpress.tailwind,
        require('@tailwindcss/typography'),
    ]
};
