
</main>

<?php do_action( 'bobines_et_melusine_content_end' ); ?>

</div>

<?php do_action( 'bobines_et_melusine_content_after' ); ?>

<footer class="bg-desert-sand" role="contentinfo">
	<nav class="mx-auto container py-4 grid grid-cols-2 md:grid-cols-3 lg:grid-cols-5 gap-4 items-start text-indian-red text-sm">
		<?php do_action( 'bobines_et_melusine_footer' ); ?>
		
		<div class="col-span-2 md:col-span-3 lg:col-span-2 flex flex-col gap-4">
			<div class="flex gap-4 items-center">
				<?php if ( has_custom_logo() ) { ?>
				<div class="h-12 w-12"><?php the_custom_logo(); ?></div>
			<?php } ?>

			<a href="<?php echo get_bloginfo( 'url' ); ?>" class="text-gray-900 font-cookie font-bold text-3xl lg:text-4xl">
				<?php echo get_bloginfo( 'name' ); ?>
			</a>
			</div>
			
			<p>
				<?php 
					// The main address pieces:
					$store_address     = get_option( 'woocommerce_store_address' );
					$store_address_2   = get_option( 'woocommerce_store_address_2' );
					$store_city        = get_option( 'woocommerce_store_city' );
					$store_postcode    = get_option( 'woocommerce_store_postcode' );

					// The country/state
					$store_raw_country = get_option( 'woocommerce_default_country' );

					// Split the country/state
					$split_country = explode( ":", $store_raw_country );

					// Country and state separated:
					$store_country = $split_country[0];
					$store_state   = $split_country[1];

					echo $store_address . "<br />";
					echo ( $store_address_2 ) ? $store_address_2 . "<br />" : '';
					echo $store_city . ', ' . $store_state . ' ' . $store_postcode . "<br />";
					echo $store_country;
				?>
			</p>
			
		</div>

		<?php

            $menu_name = 'footer';
            $locations = get_nav_menu_locations();
            $menu      = wp_get_nav_menu_object( $locations[ $menu_name ] );
            $menuitems = wp_get_nav_menu_items( $menu->term_id, array( 'order' => 'DESC' ) );

            foreach ($menuitems as $menu_item):
                if((int)$menu_item->menu_item_parent === 0): ?>
                    <ul class="flex flex-col gap-2"><li class="text-gray-900 font-medium text-lg uppercase"><a href="<?= $menu_item->url ?>"><?= $menu_item->title ?></a>
                        <?php //Check if item as subitems
                        foreach ($menuitems as $submenu_item):
                            if((int)$submenu_item->menu_item_parent === $menu_item->ID): ?>
                                <li><a href="<?= $submenu_item->url ?>"><?= $submenu_item->title ?></a></li>
                            <?php endif;
                        endforeach; ?>
                    </li></ul>
                <?php endif;
            endforeach;
			wp_reset_query(); //resetting the page query
		?>

		<div class="col-span-2 md:col-span-3 lg:col-span-5 border-t pt-4 border-indian-red flex flex-wrap gap-4 justify-between">
			<div>&copy; <?php echo date_i18n( 'Y' );?> - <?php echo get_bloginfo( 'name' );?> - Tous droits réservés - Réalisation par <?= wp_get_theme()->author; ?></div>
			<div class="flex gap-4 text-xl">
				<?php
					$social = new WP_Query( array(
						'post_type' => 'social-network',
						'meta_query' => array(
							array(
								'key' => 'texte',
								'value' => 'no text'
							)
						)
					) );
					while ( $social->have_posts() ) : $social->the_post(); ?>
						<a target="_blank" href="<?= get_field( "lien" ); ?>"><i class="<?= the_field( "icone" ); ?>"></i></a>
					<?php endwhile;
					wp_reset_query(); //resetting the page query
				?>
			</div>
		</div>
	</nav>
</footer>

</div>

<?php wp_footer(); ?>

</body>
</html>
