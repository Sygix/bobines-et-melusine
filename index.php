<?php get_header(); ?>

<?php $gallery = get_field('gallerie');
if( $gallery ): 
    if(!empty($gallery['item_1']['image'])): ?>

        <section class="bg-timberwolf">
            <!-- Swiper -->
            <div class="swiper mySwiper 2xl:mx-auto max-w-[1920px] h-[400px]">
                <div class="swiper-wrapper">

                    <?php foreach($gallery as $item): 
                        if(!empty($item['image'])): ?>

                            <div class="swiper-slide relative flex justify-center">
                                <img data-src="<?= $item['image']['url']; ?>" alt="<?= $item['image']['alt']; ?>" class="swiper-lazy h-full w-full object-cover" />
                                <div class="absolute self-center backdrop-blur-md bg-white/40 shadow-lg p-4 text-center max-w-xs md:max-w-sm lg:max-w-md">
                                    <h2 class="font-medium text-xl lg:text-2xl mb-4"><?= $item['titre']; ?></h2>
                                    <p class="mb-6 font-normal text-sm lg:text-base"><?= $item['description']; ?></p>
                                    <?php if($item['lien']): ?>
                                        <a href="<?= $item['lien']['url']; ?>" class="font-medium uppercase px-4 py-2 rounded-full shadow-lg bg-indian-red hover:bg-desert-sand text-white hover:text-gray-900 transition-colors duration-300"><?= $item['lien']['title']; ?></a>
                                    <?php endif; ?>
                                </div>
                            </div>
                    
                        <?php endif;
                    endforeach; ?>
                    
                </div>
                <div class="swiper-pagination"></div>
            </div>
        </section>

    <?php endif;
endif; ?>

<?php $grid = get_field('grille');
if( $grid ): 
    if(!empty($grid['item_1']['image'])): ?>

        <section class="bg-purple-navy">
            <div class="mx-auto container py-8 grid lg:grid-cols-2 gap-8">

                <?php $count = 0; ?>
                <?php foreach($grid as $item): 
                    if(!empty($item['image'])): 
                        $count++;
                        $anim = 'rotateXYRight';
                        if($count % 2) $anim = 'rotateXYLeft'; ?>
                        <div class="flex flex-col gap-4 odd:lg:flex-col-reverse">
                            <div class="lg:mx-auto lg:max-w-lg">
                                <h2 class="font-medium text-desert-sand text-xl lg:text-2xl mb-4 text-center lg:text-left" ><?= $item['titre']; ?></h2>
                                <p class="font-normal text-white text-sm lg:text-base mb-4" ><?= $item['description']; ?></p>
                            </div>
                            <img class="aos mx-auto w-full max-w-lg max-h-56 border-2 rounded-xl border-desert-sand shadow-lg object-cover" 
                            src="<?= $item['image']['url']; ?>" alt="<?= $item['image']['alt']; ?>" loading="lazy" width="<?= $item['image']['width']; ?>" 
                            height="<?= $item['image']['height']; ?>" data-aos="<?= $anim; ?>" data-aos-once="true" data-aos-delay="300">
                        </div>

                    <?php endif;
                endforeach; ?>

            </div>
        </section>

    <?php endif;
endif; ?>

<?php $creations = get_field('creations');
if( $creations ): ?>
    <?php if(!empty($creations['produits'][0])): ?>

        <section>
            <div class="mx-auto container py-8 flex flex-col">
                <h2 class="font-medium text-xl lg:text-2xl text-center text-indian-red mb-4"><?= $creations['titre'] ?></h2>
                
                <div class="grid grid-cols-2 lg:grid-cols-4 auto-rows-fr gap-4 mb-4">

                    <?php 
                    $count = 0;
                    foreach($creations['produits'] as $product): 
                        $productData = wc_get_product( $product->ID );
                        if($productData->catalog_visibility != 'visible' || $productData->status != 'publish') continue;
                        if($count >= 9) $count = 0;
                        
                        $class = '';
                        if($count%3 == 0) $class = 'col-span-2 lg:col-span-1 row-span-2 sm:row-span-1';
                        if($count%4 == 0) $class .= ' lg:!col-span-2';

                        $link = get_permalink($productData->get_id());
                        $name = $productData->get_name();
                        $image = $productData->get_image('woocommerce_small', array( "class" => "h-full sm:max-h-60 w-full object-cover"));
                        $price = $productData->get_price();
                        $currency = get_woocommerce_currency_symbol(); ?>
                        

                        <a href="<?= $link ?>" class="<?= $class ?> aos bg-indian-red/20 rounded-2xl p-4 min-h-[10rem]" data-aos="zoom-in" data-aos-once="true"  data-aos-delay="300">
                            <div class="relative rounded-2xl overflow-hidden h-full">
                                <?= $image ?>
                                <div class="absolute bottom-0 backdrop-blur-md bg-white/40 w-full flex justify-between flex-wrap p-4">
                                    <h3 class="font-medium text-sm xs:text-lg truncate"><?= $name ?></h3>
                                    <p class="font-medium text-sm xs:text-lg truncate text-gray-600 uppercase"><?= $price.$currency ?></p>
                                </div>
                            </div>
                        </a>
                        
                        <?php $count++;
                    endforeach; ?>

                </div>
                <a href="<?= $creations['plus_lien'] ?>" class="mx-auto font-medium uppercase px-4 py-2 rounded-full shadow-lg bg-indian-red hover:bg-desert-sand text-white hover:text-gray-900 transition-colors duration-300"><?= $creations['plus'] ?></a>
            </div>
        </section>

    <?php endif; 
endif; ?>

<?php $confection = get_field('confection');
if( $confection ): ?>
    <?php if(!empty($confection['description']) || !empty($confection['description'])): ?>

        <section>
            <div class="overflow-hidden mx-auto container py-8 grid grid-cols-1 lg:grid-cols-3">
                <img class="aos mx-auto hidden lg:block" src="<?= $confection['image_gauche']['url']; ?>" alt="<?= $confection['image_gauche']['alt']; ?>" 
                loading="lazy" width="<?= $confection['image_gauche']['width']; ?>" height="<?= $confection['image_gauche']['height']; ?>"
                data-aos="slide-right" data-aos-once="true" data-aos-delay="300">
            
                <div class="flex flex-col items-center justify-center">
                    <h2 class="font-medium text-xl lg:text-2xl mb-4 text-center text-indian-red"><?= $confection['titre']; ?></h2>
                    <p class="max-w-lg font-normal text-sm lg:text-base text-center mb-4"><?= $confection['description']; ?></p>
                    <div class="flex items-center w-full text-2xl mb-4 px-8">
                        <i class="fas fa-cut text-gray-500"></i>
                        <hr class="w-full border-2 rounded-full border-gray-500 mx-4">
                        <i class="fas fa-cut text-gray-500 rotate-180"></i>
                    </div>

                    <?php if($confection['lien']): ?>
                        <a href="<?= $confection['lien']; ?>" class="mx-auto font-medium uppercase px-4 py-2 rounded-full shadow-lg bg-indian-red hover:bg-desert-sand text-white hover:text-gray-900 transition-colors duration-300"><?= $confection['lien_texte']; ?></a>
                    <?php endif; ?>
                </div>

                <img class="mx-auto hidden lg:block" src="<?= $confection['image_droite']['url']; ?>" alt="<?= $confection['image_droite']['alt']; ?>" 
                loading="lazy" width="<?= $confection['image_droite']['width']; ?>" height="<?= $confection['image_droite']['height']; ?>"
                data-aos="slide-left" data-aos-once="true" data-aos-delay="300">
            </div>
        </section>
    <?php endif; 
endif; ?>

<?php get_template_part( 'template-parts/cta', 'newsletter' ); ?>

<section>
    <div class="prose prose-headings:text-xl lg:prose-headings:text-2xl !container my-8 mx-auto">
        <?= the_content(); ?>
    </div>
</section>

<?php
get_footer();
