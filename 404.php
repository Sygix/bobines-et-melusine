<?php get_header(); ?>

<div class="container mx-auto my-8">

	<div class="text-4xl text-indian-red border-indian-red border-b pb-4 mb-4">404</div>
	<p class="text-indian-red text-2xl md:text-3xl font-light mb-8"><?php _e( 'Désolé, la page que vous recherchez est introuvable.', 'tailpress' ); ?></p>
	<a href="<?php echo get_bloginfo( 'url' ); ?>" class="font-medium bg-desert-sand px-4 py-2 rounded-full shadow-lg hover:bg-indian-red hover:text-white transition-colors duration-300">
		<?php _e( 'Retourner à l\'accueil', 'tailpress' ); ?>
	</a>

</div>

<?php
get_footer();