<?php /* Template Name: Newsletter */ ?>
<?php get_header(); ?>

<section class="bg-camel text-white">
    <div class="mx-auto container py-8 flex flex-col items-center">
        <?= the_content(); ?>
    </div>
</section>

<?php get_template_part( 'template-parts/contact-form', 'wide' ); ?>

<?php
get_footer();
