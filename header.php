<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width">
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">

	<?php wp_head(); ?>
</head>

<body <?php body_class( 'bg-white text-gray-900 antialiased' ); ?>>

<?php do_action( 'bobines_et_melusine_site_before' ); ?>

<div id="page" class="min-h-screen flex flex-col">

	<?php do_action( 'bobines_et_melusine_header' ); ?>

	<!-- Social Header -->
	<div class="bg-camel text-white text-xl">
		<nav class="mx-auto py-4 container flex justify-between">
			<div class="grid grid-flow-col gap-4">
				<?php
					$social = new WP_Query( array(
						'post_type' => 'social-network',
						'meta_query' => array(
							array(
								'key' => 'texte',
								'value' => 'no text',
								'compare' => '!='
							)
						)
					) );
					while ( $social->have_posts() ) : $social->the_post(); ?>
						<a target="_blank" href="<?= get_field( "lien" ); ?>"><i class="<?= the_field( "icone" ); ?>"></i><span class="hidden lg:inline-block text-sm font-normal align-text-top lg:ml-2"><?= the_field( "texte" ); ?></span></a>
					<?php endwhile;
					wp_reset_query(); //resetting the page query
				?>
			</div>
			<div class="grid grid-flow-col gap-4">
				<?php
					$social = new WP_Query( array(
						'post_type' => 'social-network',
						'meta_query' => array(
							array(
								'key' => 'texte',
								'value' => 'no text'
							)
						)
					) );
					while ( $social->have_posts() ) : $social->the_post(); ?>
						<a target="_blank" href="<?= get_field( "lien" ); ?>"><i class="<?= the_field( "icone" ); ?>"></i></a>
					<?php endwhile;
					wp_reset_query(); //resetting the page query
				?>
			</div>
		</nav>
	</div>

	<header class="sticky top-0 z-[500]">

		<!-- Navigation -->
		<div class="bg-desert-sand max-h-screen">
			<div class="mx-auto py-4 container flex flex-wrap lg:flex-nowrap justify-between items-center lg:items-stretch overflow-y-scroll lg:overflow-y-visible no-scrollbar max-h-inherit ">
			
				<div class="flex gap-4 items-center">
					<?php if ( has_custom_logo() ) { ?>
						<div class="h-12 w-12"><?php the_custom_logo(); ?></div>
					<?php } ?>

					<div>
						
						<a href="<?php echo get_bloginfo( 'url' ); ?>" class="font-cookie font-bold text-3xl lg:text-4xl">
							<?php echo get_bloginfo( 'name' ); ?>
						</a>

						<?php if(is_front_page()): ?>
							<h1 class="text-sm font-light text-gray-600">
								<?php echo get_bloginfo( 'description' ); ?>
							</h1>
						<?php else: ?>
							<p class="text-sm font-light text-gray-600">
								<?php echo get_bloginfo( 'description' ); ?>
							</p>
						<?php endif; ?>
					</div>
				</div>

				<div class="lg:hidden text-3xl">
					<a href="#" aria-label="Toggle navigation" id="primary-menu-toggle">
						<i id="primary-menu-bars" class="fas fa-bars"></i>
						<i id="primary-menu-times" class="hidden fas fa-times"></i>
					</a>
				</div>

				<?php
					wp_nav_menu(
						array(
							'container'		  => 'nav',
							'container_id'    => 'primary-menu',
							'container_class' => 'hidden lg:flex basis-full lg:basis-0 h-screen lg:h-auto max-h-full p-4 lg:p-0 lg:block',
							'menu_class'      => 'lg:flex lg:-mx-4',
							'theme_location'  => 'primary',
							'li_class'        => 'relative lg:flex lg:items-center my-4 lg:my-0 text-center lg:text-left text-lg font-medium uppercase lg:mx-4 hover:text-indian-red transition-colors duration-300',
							'fallback_cb'     => false,
						)
					);
				?>

			</div>

		</div>

	</header>

	<div id="content" class="site-content flex-grow">

		<?php do_action( 'bobines_et_melusine_content_start' ); ?>

		<main>
