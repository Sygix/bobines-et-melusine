window.addEventListener('load', function () {
      // Navigation toggle
      let main_navigation = document.querySelector('#primary-menu');
      let menu_bars = document.querySelector('#primary-menu-bars');
      let menu_times = document.querySelector('#primary-menu-times');
      document.querySelector('#primary-menu-toggle').addEventListener('click', function (e) {
            e.preventDefault();
            main_navigation.classList.toggle('hidden');
            menu_bars.classList.toggle('hidden');
            menu_times.classList.toggle('hidden');
      });

      // Front Page swiper
      if (document.querySelector(".mySwiper")) {
            const swiper = new Swiper(".mySwiper", {
                  autoplay: {
                        delay: 5000,
                  },
                  slidesPerView: 1,
                  spaceBetween: 1000,
                  pagination: {
                        el: ".swiper-pagination",
                        dynamicBullets: true,
                        bulletClass: "swiper-pagination-bullet bg-indian-red h-3 w-3",
                        clickable: true,
                  },
                  // Disable preloading of all images
                  preloadImages: false,
                  // Enable lazy loading
                  lazy: true,
            });
      }

      //Leaflet MAP / MAPBOX
      if (document.querySelector("#map")) {
            let documentMap = document.querySelector("#map");

            let center = documentMap.getAttribute('data-center').split(',');
            let zoom = parseInt(documentMap.getAttribute('data-zoom'));
            let mapID = documentMap.getAttribute('data-map-id');
            let accessToken = documentMap.getAttribute('data-access-token');
            let gmapLink = documentMap.getAttribute('data-gmap-link');
            let dataMarker = documentMap.getAttribute('data-marker').split(',');

            let map = L.map('map', {
            zoomControl: false,
            scrollWheelZoom: false,
            attributionControl: false,
            center: center,
            zoom: zoom,
            });

            L.tileLayer('https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token={accessToken}', {
            id: mapID,
            tileSize: 512,
            zoomOffset: -1,
            accessToken: accessToken
            }).addTo(map);

            var myIcon = L.icon({
            iconUrl: wpObject.templateUrl+'/img/icons/marker.svg',
            iconSize: [30, 42]
            });

            var marker = L.marker(dataMarker, {icon: myIcon}).addTo(map);

            marker.on('click', (e) => {
            window.open(gmapLink, '_blank').focus();
            });

            let messageTimeout;
            documentMap.addEventListener('wheel', function (event) {

                  event.stopPropagation();
                  if (event.ctrlKey == true) {
                        event.preventDefault();
                        map.scrollWheelZoom.enable();
                        documentMap.classList.remove('map-scroll');
                        setTimeout(function(){
                              map.scrollWheelZoom.disable();
                        }, 1000);
                  } else {
                        map.scrollWheelZoom.disable();
                        documentMap.classList.add('map-scroll');
                        clearTimeout(messageTimeout);
                        messageTimeout = setTimeout(() => {
                              documentMap.classList.remove('map-scroll');
                        }, 3000);
                  }
            });

            window.addEventListener('wheel', function (event) {
                  documentMap.classList.remove('map-scroll');
            })
      }

      // aos
      if (document.querySelector(".aos")) {
            AOS.init();
      }
});