<section class="bg-purple-navy text-white">

    <?php if(!empty(get_option('contactformwide_shortcode'))) : ?>
                <?= do_shortcode( get_option('contactformwide_shortcode') ); ?>
    <?php endif; ?>

    <!-- <form action="#" class="mx-auto max-w-2xl p-8 rounded-2xl flex flex-col items-center gap-4">
        <h3 class="w-full font-medium text-xl lg:text-2xl text-center text-desert-sand">Contact us</h3>
        <p class="w-full font-normal text-sm lg:text-base text-center">Feel free to contact us if you need any assistance, any help or another question.
        </p>
        <input type="text" class="w-full rounded-3xl border-2 border-desert-sand py-2 px-4 placeholder:text-gray-500 text-gray-900 shadow-lg" id="name" placeholder="Name" required>
        <input type="email" class="w-full rounded-3xl border-2 border-desert-sand py-2 px-4 placeholder:text-gray-500 text-gray-900 shadow-lg" placeholder="Email" required>
        <textarea id="message" class="w-full rounded-3xl border-2 border-desert-sand py-2 px-4 h-36 min-h-[2.5rem] max-h-96 placeholder:text-gray-500 text-gray-900 shadow-lg" placeholder="Message" required></textarea>
        <input class="w-fit rounded-3xl border-2 border-desert-sand py-2 px-4 bg-white text-purple-navy shadow-lg font-medium transition-transform duration-300 hover:scale-110" type="submit" value="Envoyer">
    </form> -->
</section>