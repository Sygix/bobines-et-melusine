<section class="not-prose bg-camel text-white">
    <div class="mx-auto container py-8 flex flex-col items-center">
               
        <?php if(!empty(get_option('newsletter_shortcode'))) : ?>
            <?= do_shortcode( get_option('newsletter_shortcode') ); ?>
        <?php endif; ?>

        <!-- <h3 class="font-medium text-xl lg:text-2xl text-center mb-4"><i class="fas fa-envelope mr-2"></i>Ne ratez pas notre newsletter</h3>
        <p class="font-normal text-sm lg:text-base text-center mb-4">Lorem, ipsum dolor sit amet consectetur adipisicing elit.</p>
        <form class="bg-white rounded-full px-4 py-3 w-full max-w-xl flex gap-4 justify-between shadow-lg" action="" >
            <input class="w-full placeholder:text-gray-500 text-gray-900" type="text" placeholder="Entrez votre adresse email">
            <button class="p-2 rounded-full bg-camel hover:bg-camel/60 transition-colors duration-300 w-10 h-10 shrink-0" type="submit"><i class="fas fa-arrow-right"></i></button>
        </form> -->
    </div>
</section>