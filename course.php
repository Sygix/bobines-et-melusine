<?php /* Template Name: Cours & Convivialite */ ?>
<?php get_header(); ?>

<section>
    <div class="prose prose-headings:text-xl lg:prose-headings:text-2xl !container my-8 mx-auto">
        <?= the_content(); ?>
    </div>
</section>

<?php get_template_part( 'template-parts/cta', 'newsletter' ); ?>

<?php
get_footer();
