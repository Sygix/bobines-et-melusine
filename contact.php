<?php /* Template Name: Contact */ ?>
<?php get_header(); ?>

<section>
    <div class="prose prose-headings:text-xl lg:prose-headings:text-2xl !container my-8 mx-auto">
        <?= the_content(); ?>
    </div>
</section>


<section class="relative">

    <?php $map = get_field('map');
    if( $map ):
        if(!empty($map['map_id'])): ?>

            <div id="map" class="hidden lg:block h-[900px]"
                data-center="<?= $map['centre'] ?>"
                data-zoom="<?= $map['zoom'] ?>"
                data-map-id="<?= $map['map_id'] ?>"
                data-access-token="<?= $map['access_token'] ?>"
                data-marker="<?= $map['marker'] ?>"
                data-gmap-link="<?= $map['google_map_lien'] ?>"
            ></div>

        <?php endif;
    endif; ?>


    <div class="lg:absolute lg:top-8 lg:z-[450] w-full lg:w-fit">
        <div class="aos mx-auto container py-8" data-aos="slide-right" data-aos-once="true" data-aos-delay="300">
            
            <?php get_template_part( 'template-parts/contact-form' ); ?>
            
            <?php if( $map ):
                if(!empty($map['google_map_lien'])): ?>
                    <a href="<?= $map['google_map_lien']; ?>" target="_blank" class="lg:hidden mt-8 shadow-lg flex gap-4 text-white justify-center items-center py-2 px-4 font-medium rounded-full bg-indian-red hover:bg-indian-red/75 transition-colors duration-300"><span class="gmap-icon text-3xl"></span><?= get_field('btn_text'); ?></a>
                <?php endif;
            endif; ?>
        </div>
    
    </div>
</section>

<?php get_template_part( 'template-parts/cta', 'newsletter' ); ?>

<?php
get_footer();
